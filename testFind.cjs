const arr = require("./ArrayFile.cjs");
const finFunction = require("./find.cjs");

const cb = (elementAray, ele) => {
  for (let element = 0; element < elementAray.length; element++) {
    if (elementAray[element] === ele) {
      return true;
    }
  }

  return false;
};

console.log(finFunction(arr, cb));
