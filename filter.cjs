const filter = (elements, cb) => {

    if(!Array.isArray(elements) || typeof cb !== 'function' || elements.length === 0){
        return []
    }

  let flag = false;
  let arr = [];

  for (let ele = 0; ele < elements.length; ele++) {

    if (cb(elements[ele],ele,elements) === true) {
      flag = true;

      arr.push(elements[ele]);
    }
  }
  return flag === true ? arr : [];
};

module.exports = filter;
