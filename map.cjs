const mapFunction = (items, cb) => {

    if(!Array.isArray(items) || typeof cb !== 'function' || items.length === 0){
        return []
    }

    let arr = []
  
  for (let index = 0; index < items.length; index++) {
    arr.push(cb(items[index], index, items));
  }

  return arr;
};

module.exports = mapFunction;
