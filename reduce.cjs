const reduce = (elements, cb, startingValue) => {

    if(!Array.isArray(elements) || typeof cb !== 'function' || elements.length === 0){
        return []
    }


  let sum = startingValue,value = 0;

  if (startingValue === undefined) {
    sum  = elements[0];
    value = 1;
  }

  for (value; value < elements.length; value++) {

    sum = cb(sum, elements[value],value ,elements);
    
  }

  return sum;
};

module.exports = reduce;
